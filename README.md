# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Application Name: Swift Poll
Team Members:
•	Sanjay Bedudoori – S528106
•	Sai Venkat Poorna Chandu Bhogireddy – S528109
•	Venkata Prasant Poodipeddi – S528167
•	Bapuji Dirisala – S528119

Idea of the Project:

 Often Professors ask students to raise their hands for a poll. To make it digital a professor can open the application, enter a unique code and post a question with the options. Students can enter the unique code and select the desired options. The result will be shown live to the professor with total count and a graph. This application allows an individual user to create an account and also stores the poll history.
This application is not restricted only to classroom polls.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact