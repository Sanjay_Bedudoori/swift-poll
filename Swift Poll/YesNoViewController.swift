//
//  YesNoViewController.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 3/30/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//
import Parse
import Bolts
import UIKit

class YesNoViewController: UIViewController {

    @IBOutlet weak var pollID: UITextField!
    
    @IBOutlet weak var questionTF: UITextField!
    
    @IBOutlet weak var answerTF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    @IBAction func submit(_ sender: Any) {
        let yesNo = YesNo()
        yesNo.question = questionTF.text!
        yesNo.answer = answerTF.text!
        yesNo.pollID = pollID.text!
        
        
        yesNo.saveInBackground(block: { (success, error) -> Void in
            if success {
                self.displayAlert("Success!",message:"Poll saved.")
            } else {
                print(error!)
            }
        })
    }
    
    func displayAlert(_ title:String, message:String) {
        let alert = UIAlertController(title: title, message: message,preferredStyle: .alert)
        let defaultAction = UIAlertAction(title:"OK",style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert,animated:true, completion:nil)
    }
    
    
    
    
    
    // MARK: - Navigation

    //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let finalView = segue.destination as! FinalPage
        finalView.localPollID = pollID.text!
        
    }
 

}
