//
//  FinalPage.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 3/5/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//
import Parse

import UIKit


// Class to display the created question and the poll results
class FinalPage: UIViewController {

    var timer: Timer!
    
    var localPollID:String!
    
    var results:[PFObject] = []
    
    var polls:[PFObject] = []
    
    var counter1:Int = 0
    
    var counter2:Int = 0
    
    var counter3:Int = 0
    
    var counter4:Int = 0
    
    @IBOutlet weak var progressView1: UIProgressView!
    
    
    @IBOutlet weak var progressView2: UIProgressView!
    
    
    @IBOutlet weak var progressView3: UIProgressView!
    
    
    @IBOutlet weak var progressView4: UIProgressView!
    
    
    @IBOutlet weak var result1: UILabel!
    
    
    @IBOutlet weak var result2: UILabel!
    
    
    @IBOutlet weak var result3: UILabel!
    
    
    @IBOutlet weak var result4: UILabel!
    
    @IBOutlet weak var option1: UILabel!
    
    
    @IBOutlet weak var option2: UILabel!
    
    
    @IBOutlet weak var option3: UILabel!
    
    
    @IBOutlet weak var option4: UILabel!
    
    @IBOutlet weak var questionText: UILabel!
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var pollID: UILabel!
    
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    // To display Poll ID and trigger the timer to refresh the page after a time period of 1 sec
    override func viewWillAppear(_ animated: Bool) {
        
        pollID.text = "Your poll id is \(localPollID!)"
        
        getResults()
        
        getQuestionAndAnswer()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        
        progressView1.transform = progressView1.transform.scaledBy(x: 1, y: 3)
        
        progressView2.transform = progressView2.transform.scaledBy(x: 1, y: 3)
        
        progressView3.transform = progressView3.transform.scaledBy(x: 1, y: 3)
        
        progressView4.transform = progressView4.transform.scaledBy(x: 1, y: 3)
       
    }
    // To calculate and retreive the results
    func runTimedCode() {
        
        calculateResult()
        
        getResults()
        
    }
   // To retreive the results from the database
    func getResults(){
        
        let query = PFQuery(className:"Result")
        
        
        
        query.whereKey("pollID", equalTo: localPollID) // just fetch objects with this key (which is all of them)
        
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
          
                self.results = objects!
                
                self.calculateResult()
                
                // Do something with the found objects
                
            } else {
      
            }
        })
    }
    
    // To count the polls enterd by the user
    func calculateResult(){
        
             counter1 = 0
        
             counter2 = 0
        
             counter3 = 0
        
             counter4 = 0
        
        for result in self.results{
            
            
            if result["selectedchoice"] as! String! == option1.text{
                
                counter1 = counter1 + 1
                
            }
            if result["selectedchoice"] as! String! == option2.text{
                
                counter2 = counter2 + 1
                
            }
            if result["selectedchoice"] as! String! == option3.text{
                
                counter3 = counter3 + 1
                
            }
            if result["selectedchoice"] as! String! == option4.text{
                
                counter4 = counter4 + 1
                
            }
            
        }
        if results.count == 0 {
            
            if option3.text == "" {
                
                result3.isHidden = true
                
                progressView3.isHidden = true
                
            }
            if option4.text == ""{
                
                result4.isHidden = true
                
                progressView4.isHidden = true
                
            }
            
            return
        }
        
        result1.text = "\(counter1)"
        
        result2.text = "\(counter2)"
        
        progressView1.progress = Float(counter1)/Float(results.count)
        
        progressView2.progress = Float(counter2)/Float(results.count)
        
        result3.text = "\(counter3)"
        
        progressView3.progress = Float(counter3)/Float(results.count)
        
        if option3.text == "" {
            
            result3.isHidden = true
            
            progressView3.isHidden = true
            
        }
        
        result4.text = "\(counter4)"
        
        progressView4.progress = Float(counter4)/Float(results.count)
        
        if option4.text == ""{
            
            result4.isHidden = true
            
            progressView4.isHidden = true
            
        }
        
    }
    
    //To display the question and the choices created by the host
    func getQuestionAndAnswer(){
        
        let query = PFQuery(className:"Poll")
        
        query.whereKey("pollID", equalTo: localPollID) // just fetch objects with this key (which is all of them)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.
                self.polls = objects!
                // Do something with the found objects
                let poll:PFObject = self.polls[0]
                
                self.questionText.text = poll["pollQuestion"] as! String!
                
                self.option1.text = poll["choice1"] as! String!
                
                self.option2.text = poll["choice2"] as! String!
                
                self.option3.text = poll["choice3"] as! String!
                
                self.option4.text = poll["choice4"] as! String!
                
                self.calculateResult()
                
            } else {
                
                // Log details of the failure
                //self.alert(message: "Oops", title: "error")
            }
        })
    }
    
    //Button to logout
    @IBAction func logoutBTN(_ sender: Any) {
        
        PFUser.logOut()
        
    }
    // To deactivate the timer when the user goes to another screen
    override func viewWillDisappear(_ animated: Bool) {
        
        timer.invalidate()
        
    }
    }
    

    



