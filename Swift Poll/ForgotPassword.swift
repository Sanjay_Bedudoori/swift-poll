//
//  ForgotPassword.swift
//  Swift Poll
//
//  Created by Bhogireddy,Sai Venkat Poorna Chandu on 4/2/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//

import UIKit

import Parse

class ForgotPassword: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var emailTB: UITextField!
    
    // Button to Submit
    @IBAction func submitBTN(_ sender: Any) {
        
        if (emailTB.text?.isEmpty)!{
            
            let myAlert = UIAlertController(title:"Alert",message:"Please enter your email ID",preferredStyle:UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
            
            myAlert.addAction(okAction)
            
            self.present(myAlert,animated: true,completion: nil)
            
            return
        }
        
        PFUser.requestPasswordResetForEmail(inBackground: emailTB.text!) { (success, error: Error?) -> Void in
            
            if (success){
                
                self.performSegue(withIdentifier: "success", sender: nil)
                
                return

        }

        if(error != nil){
                
        }
            
    }
        
    }
}
