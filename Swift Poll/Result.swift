//
//  Result.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 4/4/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//

import Foundation

import Parse

import Bolts

class Result:PFObject, PFSubclassing{
    
    @NSManaged var pollQuestion:String
    
    @NSManaged var selectedchoice:String?
    
    @NSManaged var pollID:String
    
    @NSManaged var deviceID:String
    
    static func parseClassName() -> String{
        return "Result"
    }
    
    
}
