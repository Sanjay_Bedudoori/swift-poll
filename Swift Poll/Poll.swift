//
//  Poll.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 4/2/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//

import Parse

import Bolts

import Foundation

class Poll:PFObject, PFSubclassing{
    
    @NSManaged var pollQuestion:String
    
    @NSManaged var choice1:String?
    
    @NSManaged var choice2:String?
    
    @NSManaged var choice3:String?
    
    @NSManaged var choice4:String?
    
    @NSManaged var pollID:String
    
    @NSManaged var userID:String
    
    static func parseClassName() -> String{
        return "Poll"
    }
    
    
}
