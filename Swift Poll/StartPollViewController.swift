//
//  StartPollViewController.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 3/5/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//
import Parse
import UIKit
// Class for login Screen
class StartPollViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var emailTB: UITextField!
    
    
    @IBOutlet weak var passwordTB: UITextField!
    
    // To display an alert
    func alert(message: NSString, title: NSString) {
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    // Button for login
    @IBAction func loginBTN(_ sender: Any) {
        
        if (emailTB.text?.isEmpty)! || (passwordTB.text?.isEmpty)!{
            
            let myAlert = UIAlertController(title:"Alert",message:"All fields are required to fill in",preferredStyle:UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
            
            myAlert.addAction(okAction)
            
            self.present(myAlert,animated: true,completion: nil)
            
            return
        }
        let username = emailTB.text
        
        let password = passwordTB.text
        
        // Defining the user object
        PFUser.logInWithUsername(inBackground: username!, password: password!, block: {(user, error) -> Void in
            
            if let error = error as NSError? {
                
                let errorString = error.userInfo["error"] as? NSString
                
                let myAlert = UIAlertController(title:"Alert",message: errorString as String?,preferredStyle:UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
                
                myAlert.addAction(okAction)
                
                self.present(myAlert,animated: true,completion: nil)
                
                return

               
            }
            else {
                
                self.performSegue(withIdentifier: "CreatePoll", sender: nil)
                
            }
        })

    }
    
    // Go to home screen
    @IBAction func goHome(_ sender: Any) {
        
    }
    // Unwind segue from register screen
    @IBAction func unwindFromRegister(for unwindSegue: UIStoryboardSegue) {
    }
    
        

}
