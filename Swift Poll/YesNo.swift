//
//  YesNo.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 3/30/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//

import Foundation
import Parse
import Bolts

class YesNo:PFObject, PFSubclassing{
    
    @NSManaged var question:String
    @NSManaged var answer:String
    @NSManaged var pollID:String
    
    static func parseClassName() -> String{
        return "YesNo"
    }
    
}
