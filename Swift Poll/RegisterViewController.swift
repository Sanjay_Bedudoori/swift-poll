//
//  RegisterViewController.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 3/27/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//
import Parse
import UIKit
// Class for account registration
class RegisterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var emailTB: UITextField!
    
    
    @IBOutlet weak var passwordTB: UITextField!
    
    
    @IBOutlet weak var confirmTB: UITextField!
    
    
   
    
    @IBOutlet weak var lastName: UITextField!
    
    // To display an alert
    func alert(message: NSString, title: NSString) {
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    // Button to register the host account
    @IBAction func signUp(_ sender: Any) {
        
            if (emailTB.text?.isEmpty)! || (passwordTB.text?.isEmpty)! || (confirmTB.text?.isEmpty)! || (lastName.text?.isEmpty)!{
                let myAlert = UIAlertController(title:"Alert",message:"All fields are required to fill in",preferredStyle:UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
                
                myAlert.addAction(okAction)
                
                self.present(myAlert,animated: true,completion: nil)
                
                return
            }
            
            if passwordTB.text != confirmTB.text{
                let myAlert = UIAlertController(title:"Alert",message:"Passwords do not match. Please try again",preferredStyle:UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
                
                myAlert.addAction(okAction)
                
                self.present(myAlert,animated: true,completion: nil)
                
                return
            }
            
            
            let email = emailTB.text
            let password = passwordTB.text
            
        
            let lName = lastName.text
            // Defining the user object
            let user = PFUser()
            user.username = lName!
            user.email = email
            user.password = password
            
            // We won't set the email for this example;
            // Just for simplicity
            
            // Signing up using the Parse API
            user.signUpInBackground {(success, error) -> Void in
                
                var userMessage = "Registration is successful. Thank you!"
                
                if(!success){
                    userMessage = error!.localizedDescription
                    let myAlert = UIAlertController(title:"Alert",message:userMessage,preferredStyle:UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
                    
                    myAlert.addAction(okAction)
                    
                    self.present(myAlert,animated: true,completion: nil)
                    
                    return
                    
                }
                else{
                    self.performSegue(withIdentifier: "Registered", sender: nil)
                }
                
            }
            
            

    }
    // To close the keyboard when the user clicks anywhere on the screen outside the keyboard
    override func viewWillAppear(_ animated: Bool) {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(NewPollViewController.dismissKeyboard))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)

    }
    // To close the keyboard when the user clicks anywhere on the screen outside the keyboard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    //Button to go back to home screen
    @IBAction func toHome(_ sender: Any) {
    }
    
   
}
    


