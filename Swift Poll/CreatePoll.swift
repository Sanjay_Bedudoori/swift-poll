//
//  CreatePoll.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 3/5/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//
import Parse

import UIKit

import Bolts
// to create a poll
class CreatePoll: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var polls:[PFObject] = []
    
    var pollID:String!
    
    @IBOutlet weak var pollView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return polls.count
        
    }
    // To display the poll history from the database
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let poll:PFObject = polls[(indexPath as NSIndexPath).row]
        
        cell.textLabel!.text = poll["pollQuestion"] as! String!
        
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little pre5paration before navigation
    //Segue to go the final page which displays poll results
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FinalPage"{
            
            let row = self.pollView.indexPathForSelectedRow?.row
            
            let poll:PFObject = polls[row!]
            
            pollID = poll["pollID"] as! String!
            
            let destination = segue.destination as! FinalPage
            
            destination.localPollID = pollID

        }
        else{
            
        }
            }
 
    
    override func viewWillAppear(_ animated: Bool) {

        self.getPolls()
    }
    // To display an alert
    func alert(message: NSString, title: NSString) {
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    //To retreive polls from the database
    func getPolls(){
        
        let query = PFQuery(className:"Poll")
       
        
        
            query.whereKey("userID", equalTo: PFUser.current()?.objectId! as Any) // just fetch objects with this key (which is all of them)
       
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                                self.polls = objects!
                // Do something with the found objects
                self.pollView.reloadData()
            } else {
                // Log details of the failure
                self.alert(message: "Oops", title: "error")
            }
        })
    }
    
    // Unwind segue from the Question Screen
    @IBAction func unwindFromQuestion(segue: UIStoryboardSegue){
        
    }
    
    // Function to logout
    @IBAction func logoutBTN(_ sender: Any) {
        
        PFUser.logOut()
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete{
            
            var query = PFObject(className:"Poll")
            
            query = polls[indexPath.row]
            
            
            
            do{
                
                try query.delete()
                
                polls.remove(at: indexPath.row)
                
                tableView.reloadData()
                
            }
                
            catch{
                
                print(error)
                
            }
        }
        
    }
    
}
