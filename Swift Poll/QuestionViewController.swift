//
//  QuestionViewController.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 4/4/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//
import Parse

import UIKit

import Bolts
// To display the question and the options created by the host
class QuestionViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    
    var polls:[PFObject] = []

    
     var pollQuestion:String!
    
     var choice1:String!
    
     var choice2:String!
    
    var choice3:String!
    
    var choice4:String!
    
    var pollID:String!
    
    var pickerDataSource:Array<String>!
    
    var selectedRow:Int = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.options.dataSource = self
        
        self.options.delegate = self
        
        // Do any additional setup after loading the view.
        self.displayPollQuestion()
            }

    
    @IBOutlet weak var options: UIPickerView!
    
    @IBOutlet weak var question: UILabel!
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        // To display question and choices
        let poll:PFObject = self.polls[0]
        
        self.question.text = poll["pollQuestion"] as! String!
        self.choice1 = poll["choice1"] as! String!
        self.choice2 = poll["choice2"] as! String!
        self.choice3 = poll["choice3"] as! String!
        self.choice4 = poll["choice4"] as! String!
        if self.choice3.isEmpty {
            self.pickerDataSource = [self.choice1,self.choice2]
        }
        else if self.choice4.isEmpty{
            self.pickerDataSource = [self.choice1,self.choice2,self.choice3]
        }
        else{
            self.pickerDataSource = [self.choice1,self.choice2,self.choice3,self.choice4]
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerDataSource.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickerDataSource[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(row == 0)
        {
            selectedRow = row
        }
        else if(row == 1)
        {
            selectedRow = row
        }
        else if(row == 2)
        {
            selectedRow = row
        }
        else
        {
            selectedRow = row
        }
    }
    // To submit the poll and enter into the database
    @IBAction func submitPoll(_ sender: Any) {
        let result = Result()
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        result.pollID = pollID
        result.deviceID = deviceID
        
        result.selectedchoice = pickerDataSource[selectedRow]
        
        
        
        result.saveInBackground(block: { (success, error) -> Void in
            
            if success {
                
                self.displayAlertWithTitle("Success!", message:"Your Choice saved.")
                
            }
                
            else{
                
                print(error!)
                
            }
            
        })
    }
    // To display an alert
    func displayAlertWithTitle(_ title:String, message:String){
        
        let alert:UIAlertController = UIAlertController(title: title, message: message,
                                                        preferredStyle: .alert)
        
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(defaultAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    // To display an alert
    func alert(message: NSString, title: NSString) {
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    // To display the poll question
    func displayPollQuestion() {
        let query = PFQuery(className:"Poll")
        
        query.whereKey("pollID", equalTo:pollID)
        
        query.limit = 1
        

        do{
            self.polls = try query.findObjects()
            
        }
        catch{
            print(error)
        }
        
        
       
    }
    
}
