//
//  ViewController.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 3/5/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//
import Parse

import UIKit

/*
   Class for initial Home screen contains two buttons - "Host A Poll" and "Join A Poll"
 
*/

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
         
        UnwindSegue from join poll Screen
    
    */
    
    @IBAction func unwindFromJoinPoll(segue: UIStoryboardSegue){
        
    }
 
    /*
     
     Unwind from Polls Screen
     
     */
    
    @IBAction func unwindFromPolls(segue: UIStoryboardSegue){
        
    }
    
    /*
        Segue to Login Screen, If user clicks on Host A Poll
    */
    @IBAction func toHostPoll(_ sender: Any) {
        
        if PFUser.current() == nil {
            self.performSegue(withIdentifier: "loginScreen", sender: nil)
        }
        else{
            self.performSegue(withIdentifier: "CreatePoll", sender: nil)
        }
    }
   
}

