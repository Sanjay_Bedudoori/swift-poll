//
//  SuccessViewController.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 4/10/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.hidesBackButton = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Home button to take the user back to the home screen
    @IBAction func toHost(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main",bundle:nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "viewController") 
        self.present(vc, animated: true, completion:nil)
    }
    
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
