//
//  NewPollViewController.swift
//  Swift Poll
//
//  Created by Bhogireddy,Sai Venkat Poorna Chandu on 4/2/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//

import Parse
import Bolts
import UIKit
//  To create a poll by entering in the poll question and the choices
class NewPollViewController: UIViewController {
    
    var randomID:String!
    
    @IBOutlet weak var pollQuestion: UITextView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var choice1: UITextField?
    
    
    @IBOutlet weak var choice2: UITextField?
    
    
    @IBOutlet weak var choice3: UITextField?
    
    
    @IBOutlet weak var choice4: UITextField?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        scrollView.contentSize.height = 1000
        
    }
    // To close the keyboard when the user clicks outside the keyboard window
    override func viewWillAppear(_ animated: Bool) {
        
        let tapper = UITapGestureRecognizer(target: self, action:#selector(NewPollViewController.dismissKeyboard))
        
        tapper.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tapper)
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    func dismissKeyboard() {
        
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        
        view.endEditing(true)
        
    }
    //Button to create a poll
    @IBAction func createPoll(_ sender: Any) {
        
        if pollQuestion.text.isEmpty || (choice1?.text?.isEmpty)! || (choice2?.text?.isEmpty)!{
            
            let myAlert = UIAlertController(title:"Alert",message:"Please enter your Question and first two choices",preferredStyle:UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
            
            myAlert.addAction(okAction)
            
            self.present(myAlert,animated: true,completion: nil)
            
            return
        }
        if pollQuestion.text.isEmpty == false || (choice1?.text?.isEmpty)! == false || (choice2?.text?.isEmpty)! == false{
            
            if (choice3?.text?.isEmpty)! && choice4?.text?.isEmpty == false{
                
                let myAlert = UIAlertController(title:"Alert",message:"Please enter choice3 first",preferredStyle:UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
                
                myAlert.addAction(okAction)
                
                self.present(myAlert,animated: true,completion: nil)
                
                return
                
            }

        }
        
        let poll = Poll()
        
        poll.pollQuestion = pollQuestion.text!
        
        poll.choice1 = choice1?.text
        
        poll.choice2 = choice2?.text
        
        poll.choice3 = choice3?.text
        
        poll.choice4 = choice4?.text
        
        poll.pollID = randomString(length: 6)
        
        poll.userID = (PFUser.current()?.objectId)!
        
        randomID = poll.pollID
        
        poll.saveInBackground(block: { (success, error) -> Void in
            
            if success {
                
                self.displayAlertWithTitle("Success!", message:"Poll saved.")
                
            }
                
            else{
                
                print(error!)
                
            }
            
        })

        
    }
    
    // To generate a random poll ID
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            
            let rand = arc4random_uniform(len)
            
            var nextChar = letters.character(at: Int(rand))
            
            randomString += NSString(characters: &nextChar, length: 1) as String
            
        }
        
        return randomString
        
    }
    
    
    // To display the alert with title
    func displayAlertWithTitle(_ title:String, message:String){
        
        let alert:UIAlertController = UIAlertController(title: title, message: message,
                                                        preferredStyle: .alert)
        
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(defaultAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    // Segue to go to the resuts page
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       if segue.identifier == "FinalPage" {
        
            let finalPage = segue.destination as! FinalPage
            
            finalPage.localPollID = randomID

       }
        
        
    }
    // Button to logout
    @IBAction func logoutBTN(_ sender: Any) {
        
        PFUser.logOut()
    }
    
    
    
}
