//
//  JoinPoll.swift
//  Swift Poll
//
//  Created by Bedudoori,Sanjay on 3/5/17.
//  Copyright © 2017 Bedudoori,Sanjay. All rights reserved.
//
import Parse
import Bolts
import UIKit
// Class for Join a Poll screen
class JoinPoll: UIViewController {

    @IBOutlet weak var pollID: UITextField!
    
    var polls:[PFObject]?
    
    var results:[PFObject] = []
    
    var pollQuestion:String!
    
    var choice1:String!
    
    var choice2:String!
    
    var choice3:String!
    
    var choice4:String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getPolls()
        // To close the keyboard when user clicks anywhere on screen apart from the keyboard
        let tapper = UITapGestureRecognizer(target: self, action:#selector(NewPollViewController.dismissKeyboard))
        
        tapper.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tapper)
        
    }
    
    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        
    }
    // To check if the user has already submitted the response. 
    // This function is also used for the validation of poll ID
    @IBAction func join(_ sender: Any) {
        
        getPolls()
        
        uniquePoll()
        
        if results.count > 0{
            
            let myAlert = UIAlertController(title:"Alert",message:"You have already submitted your response",preferredStyle:UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
            
            myAlert.addAction(okAction)
            
            self.present(myAlert,animated: true,completion: nil)
            
            return

        }
        print("Poll count in join: \(polls?.count)")
        
        if polls?.count == nil || polls?.count == 0{
            
            let myAlert = UIAlertController(title:"Alert",message:"Incorrect poll ID.",preferredStyle:UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title:"Ok",style:UIAlertActionStyle.default,handler:nil)
            
            myAlert.addAction(okAction)
            
            self.present(myAlert,animated: true,completion: nil)
            
            return
        }
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    // Segue to the Question View Controller
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    if segue.identifier == "Question" {
        
        let questionViewController = segue.destination as! QuestionViewController
        
        if pollID.text!.characters.count > 0 {
            
            questionViewController.pollID = pollID.text!
            
        }
        

    }
    
      }
    // Method to Check if the poll ID is unique
    func uniquePoll(){
        
        let query = PFQuery(className:"Result")
        
         let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        query.whereKey("deviceID", equalTo: deviceID ) // just fetch objects with this key (which is all of them)
        
        query.whereKey("pollID", equalTo: pollID.text!)
        
        do{
            self.results = try query.findObjects()
            
        }
        catch{
            print(error)
        }
    
        

    }
    // To retrieve poll history from the database
    func getPolls(){
        
        let query = PFQuery(className:"Poll")
        
        
        
        query.whereKey("pollID", equalTo: pollID.text!) // just fetch objects with this key (which is all of them)
        
        do{
            self.polls = try query.findObjects()
            
        }
        catch{
            print(error)
        }


        
    }
    
    // Method to display any alert
    func displayAlert(_ title:String, message:String) {
        
        let alert = UIAlertController(title: title, message: message,preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title:"OK",style: .default, handler: nil)
        
        alert.addAction(defaultAction)
        
        self.present(alert,animated:true, completion:nil)
        
    }
    
    
}
